import React from "react";
import "./message.scss";

export const Message = (props) => {
  const { avatar, user, text, createdAt } = props;
  const date = new Date(createdAt);
  const options = {
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
  };
  console.log(date);
  const time = new Intl.DateTimeFormat("en-US", options).format(date);
  console.log(time);
  return (
    <div className="message">
      <div className="message-user-avatar">
        <img src={avatar} alt="avatar" />
      </div>
      <div className="message-info">
        <p className="message-user-name">{user}</p>
        <p className="message-text">{text}</p>
      </div>
      <span className="message-time">{time}</span>
    </div>
  );
};
