import React from "react";
import "./header.scss";

export const Header = ({ chat }) => {
  const uniqueUsers = [...new Set(chat.map((item) => item.user))];
  return (
    <header className="header">
      <div className="wrapper">
        <h2 className="header-title">Chat App</h2>
        <h2 className="header-title">{uniqueUsers.length} Members</h2>
      </div>
    </header>
  );
};
