import React, { useEffect, useState } from "react";
import Header from "./components/header";
import MessageList from "./components/messageList";
import "./chat.scss";

const URL = "https://edikdolynskyi.github.io/react_sources/messages.json";

const Chat = () => {
  const [chatState, setChatState] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const fetchData = async () => {
      try {
        const response = await fetch(URL);
        if (!response.ok) {
          throw new Error(response.message);
        }
        const data = await response.json();
        if (data.length) {
          setIsLoading(false);
        }
        setChatState(data);
      } catch (err) {
        console.log(err);
      }
    };
    fetchData();
  }, []);
  return (
    <div className="chat-container">
      <Header chat={chatState} />
      <div className="wrapper">
        <MessageList chat={chatState} />
      </div>
    </div>
  );
};

export default Chat;
