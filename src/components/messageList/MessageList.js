import React from "react";
import { Message } from "../message/Message";
import "./messageList.scss";

export const MessageList = ({ chat }) => {
  const list = chat.map((user) => {
    const { id, ...rest } = user;
    return <Message key={id} {...rest} />;
  });
  return <>{list}</>;
};
